$(document).ready(function() {
  get_profiles();
});

profile = function(first_name, last_name, email, bio, photo, website) {
    // represent the profile on the DOM
    $(".profiles").append("<img src='" + photo + "'/>")
};

get_profiles = function() {
    $.get('http://swacomm.herokuapp.com/api/members/?format=json', function(res) {
        $.each(res, function(index, val) {
            var first_name = val['first_name'];
            var last_name = val['last_name'];
            var email = val['email'];
            var bio = val['bio'];
            var photo = val['photo'];
            var website = val['website'];
            profile(first_name, last_name, email, bio, photo, website);
        });
    });
};